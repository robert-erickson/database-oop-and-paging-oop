<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include 'lib/constants.php';
        //include "lib/custom-functions.php";
        print '<title>' . ASSOCIATION_SHORT_NAME . ' -> ' . PATH_PARTS['filename'] . '</title>';
        ?>
        <meta charset="utf-8">
        <meta name="author" content="Erickson Consulting">
        <meta name="description" content="Reservation management. We make the paper work easy for you. Manage all your reservation needs.">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/custom.css?version=1.0" type="text/css" media="screen">

        <?php
// %^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
//
        // PATH SETUP

        $yourURL = DOMAIN . PHP_SELF;

// %^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
//
        // inlcude all libraries. 
// 
// %^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
        print '<!-- begin including libraries -->';

        include LIB_PATH . 'DataBase.php';

        $thisDatabaseReader = new Database('rerickso_reader', DATABASE_NAME);
        $thisDatabaseWriter = new Database('rerickso_writer', DATABASE_NAME);

        //require_once(LIB_PATH . 'Contact.php');
        require_once(LIB_PATH . 'Paging.php');
      // how many to get?
      //  $allReservations = new Reservations(?????????????????);
        print '<!-- libraries complete-->';
        ?>	

    </head>
    <!-- ################ body section ######################### -->

<?php
print '<body id="' . PATH_PARTS['filename'] . '">';
include "header.php";
include "nav.php";
