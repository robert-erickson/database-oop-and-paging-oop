Robert Erickson

this is an example of using OOP for your database connection. An example can be found: <a href="https://rerickso.w3.uvm.edu/projects/database-n-paging/" target="_blank">https://rerickso.w3.uvm.edu/projects/database-n-paging/</a>

NOTE: you would not add a method to the database class to get all the animals. You would instead make a new class file for that.
<pre>
---php
class Animals{
    var $db;

    public function __construct($db){ 
        $this->db = $db;
        return ''; 
    }

    public function getQAnimals(){
        $sql  = 'SELECT pmkWildlifeId, fldType, fldCommonName, fldDescription, fldHabitat, ';
        $sql .= 'fldReproduction, fldDiet, fldManagement, fldStatus, fldMainImage ';
        $sql .= 'FROM tblWildlife ';
        $sql .= 'ORDER BY fldCommonName';
        $records = $this->db->select($sql);
        if (!empty($records)) {
            return $records;
        }
        return ""; 
    }
} // end class 
?>
</pre>
