<?php
include 'top.php';

// sorting
$orderBy = isset($_GET["sortField"]) ? htmlspecialchars($_GET["sortField"]) : 'fldFirstName';

$sortDirection = isset($_GET["sortDirection"]) ? htmlspecialchars($_GET["sortDirection"]) :  'ASC';

$oppositeSort = $sortDirection == 'ASC' ? 'DESC' : 'ASC';

// paging 1/2
$start = isset($_GET["start"]) ? htmlspecialchars($_GET["start"]) :  0;
$numberToDisplay = 3;

$sql  = 'SELECT pmkPersonId, fldFirstName, fldMiddleInitial, fldLastName, fldBirthPlace, fldBirthPlaceState, fldHomeTown, fldHomeTownState ';
$sql .= 'FROM tblPerson ';
$sql .= 'ORDER BY ' . $orderBy . ' ' . $sortDirection;
$sql .= ' LIMIT ' . $start . ', ' . $numberToDisplay;

$data ='';

$peoples =  $thisDatabaseReader->select($sql, $data);

$sql  = 'SELECT count(pmkPersonId) as totalRecords ';
$sql .= 'FROM tblPerson ';

$totalRecords = $thisDatabaseReader->totalRecords($sql, $data);

$paging = new Paging($peoples, $orderBy, $totalRecords, $start, $numberToDisplay+1);

$start = $paging->getStart();
$nextStart = $paging->getNextStart();
$previous = $paging->getPrevious();

$total = $paging->getTotal();


$nextGetString = '?sortField=' . $orderBy . '&sortDirection=' . $sortDirection . '&start=' . $nextStart;
$previousGetString = '?sortField=' . $orderBy . '&sortDirection=' . $sortDirection . '&start=' . $previous;

?>
<main>
<h2>People</h2>
<p></p>
<table>
    <caption><?php
print '<p>Showing records ' . $start + 1;

$nextStart = ($nextStart <= $total) ? $nextStart : $total;

print ' to ' . $nextStart . ' of ' . $total . '<p>';
?></caption>
    <tr>
        <td style="text-align: left;" colspan="2"><?php print $paging->getPageStartText(); ?></td>
        <td style="text-align: right;"><?php print $paging->getPageEndText(); ?></td>
    </tr>
            <tr>
                <th><a href='?sortField=fldFirstName&amp;sortDirection=<?php print $oppositeSort. '&start=' . $start;  ?>'>First Name 
                <img class="arrow" src="<?php print ($oppositeSort == 'ASC') ? 'down-arrow.png' : 'up-arrow.png'; ?>"></a>
            </th>
                <th><a href='?sortField=fldMiddleInitial&amp;sortDirection=<?php print $oppositeSort. '&start=' . $start;  ?>'>Middle 
                <img class="arrow" src="<?php print ($oppositeSort == 'ASC') ? 'down-arrow.png' : 'up-arrow.png'; ?>"></a></th>
                <th><a href='?sortField=fldLastName&amp;sortDirection=<?php print $oppositeSort. '&start=' . $start;  ?>'>Last Name</a>
                <img class="arrow" src="<?php print ($oppositeSort == 'ASC') ? 'down-arrow.png' : 'up-arrow.png'; ?>"></a></th>
            </tr>
<?php

foreach($peoples as $people){
    print '<tr>';
    print '<td>' . $people['fldFirstName'] . '</td>';
    print '<td>' . $people['fldMiddleInitial'] . '</td>';
    print '<td>' . $people['fldLastName'] . '</td>';
    print '</tr>' . PHP_EOL;
}

print '<tr><th style="text-align: left;" colspan="2">';
print ($previous >= 0) ? '<a href="' . $previousGetString . '">Previous</a>' : '<span class="noLink">Previous</span>';
print '</th>';

print '<th style="text-align: right;">';
print ($nextStart < $total) ? ' <a href="' . $nextGetString . '">Next</a>' : '<span class="noLink">Next</span>';
print '</th></tr>';

?>
</table>
</main>
<?php include "footer.php"; ?>
</body>
</html>
