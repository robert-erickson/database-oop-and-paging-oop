<!-- *******************************    L   B  ************************  -->
<?php
define ('DEBUG', false);

// ******************    Server settings  ******************************** -->

$_SERVER = filter_input_array(INPUT_SERVER, FILTER_SANITIZE_STRING);

// sanatize GET global variables
if (!empty($_GET)) {
    $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
}
define ('SERVER', $_SERVER['SERVER_NAME']);

define ('DOMAIN', '//' . SERVER);

define ('PHP_SELF', $_SERVER['PHP_SELF']);
define ('PATH_PARTS', pathinfo(PHP_SELF));

define ('BASE_PATH', DOMAIN . PATH_PARTS['dirname'] . '/');

define ('LIB_PATH', 'lib/');

define('DATABASE_NAME', 'RERICKSO_Sample');

if (DEBUG) {
    print '<hr><h2>SERVER</h2><p>Domain: ' . DOMAIN;
    print '<p>PHP SELF: ' . PHP_SELF;
    print '<p>PATH PARTS<pre>';
    print_r(PATH_PARTS);
    print '</pre></p>';
    print '<p>BASE_PATH: ' . BASE_PATH;
    print '<p>LIB_PATH: ' . LIB_PATH;
    print '<p>DATABASE_NAME: ' . DATABASE_NAME;
}

// ******************    Club settings  ******************************** -->
define('ASSOCIATION_NAME', 'Personal Phone Book');

define('ASSOCIATION_SHORT_NAME', 'Book');

define('ADMIN_EMAIL', 'rerickso@gmail.com');

if (DEBUG) {
    print '<hr><p>NAME: ' . ASSOCIATION_NAME;
    print '<p>SHORT: ' . ASSOCIATION_SHORT_NAME;
    print '<p>ADMIN: ' . ADMIN_EMAIL;
}
?>
<!-- *******************************    L  E   ************************ -->
